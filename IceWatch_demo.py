#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 17:40:06 2022

@author: qinz
"""

from tensorflow.keras.models import *
import cv2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from IceWatch_seg import *
import orthorect


### Inputs ###########################
# Load trained model 
# our trained model is deep supervision
model = load_model('/home/qinz/Documents/PythonScripts/Unet/result/NestedUnet/nd1_6layer_60epoch_256/nestedunet_nd1_6layer_60epoch_256.hdf5')

# Read the classes and the corresponding rgb colours
# water: blue [0, 0, 255], ice: green [0, 255, 0], others (background): black [0, 0, 0]
classes = pd.read_csv('/mnt/project/metkl/ExtremeEarth/Qin/Scripts/IceWatch/class_dict.csv', index_col = 0)

# input raw image
img = cv2.imread('/home/qinz/Documents/Data/Annotation/CamVid/dataset/test_images/20180830_190615_P8300308.JPG')
# ground truth
gt = cv2.imread('/home/qinz/Documents/Data/Annotation/CamVid/dataset/test_masks/20180830_190615_P8300308.png')

# camera attitude
pitch = -60
roll = 0
yaw = 0

target_size = (256, 256)


### Processing ######################
img = cv2.resize(img, target_size)  # resize the raw image
img = cv2.cvtColor((img).astype(np.uint8), cv2.COLOR_BGR2RGB)  # convert BGR to RGB
img = np.array(np.array(img) * 1./255)  # normalise image intensity

gt = cv2.resize(gt, target_size)
gt = cv2.cvtColor((gt).astype(np.uint8), cv2.COLOR_BGR2RGB)    

# Segmentation
seg, seg_rgb = IceWatch_seg(img, model, classes)  # water: 0 / blue [0, 0, 255]; ice: 1 / green [0, 255, 0]; others (background): 2 / black [0, 0, 0]
im_rgb = seg_rgb[5].astype(np.float32)   # the output of the trained model has 6 predictions at different depths
im_seg = seg[5].astype(np.float32)       # here we choose the prediction at the depth of 6
        
# Orthorectification
I = orthorect.orthorect(img, pitch, roll, yaw, scale = 0.6, borderValue = (255, 255, 255))
I_gt = orthorect.orthorect(gt, pitch, roll, yaw, scale = 0.6, borderValue = (255, 255, 255))
I_seg = orthorect.orthorect(im_seg, pitch, roll, yaw, scale = 0.6, borderValue = 3)
I_rgb = orthorect.orthorect(im_rgb, pitch, roll, yaw, scale = 0.6, borderValue = (255, 255, 255))
        
# ice concentration (SIC)
water = np.where(I_seg == 0)  # find water pixels
p_water = len(water[0])
ice = np.where(I_seg == 1)   # find ice pixels
p_ice = len(ice[0])
IC = p_ice / (p_water + p_ice)
        
# Plot figure
plt.figure()
fig, axes = plt.subplots(nrows = 1, ncols = 4, figsize = (55, 20))
fig.tight_layout() 
plt.subplots_adjust(top = 0.95)
[axis.set_axis_off() for axis in axes.ravel()]
        
ax = fig.add_subplot(1, 4, 1)
ax.imshow(img)
ax.set_title('Original image', fontdict={'fontsize': 50, 'fontweight': 'medium'})
        
ax = fig.add_subplot(1, 4, 2)
ax.imshow(I)
ax.set_title('Rectified image', fontdict={'fontsize': 50, 'fontweight': 'medium'})
        
ax = fig.add_subplot(1, 4, 3)
ax.imshow(I_gt)
ax.set_title('Ground Truth', fontdict={'fontsize': 50, 'fontweight': 'medium'})
        
ax = fig.add_subplot(1, 4, 4)
ax.imshow(I_rgb)
ax.set_title('Final processed image, SIC = %f'%IC, fontdict={'fontsize': 50, 'fontweight': 'medium'})
