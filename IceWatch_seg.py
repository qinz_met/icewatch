#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 17:40:06 2022

@author: qinz
"""

from tensorflow.keras.models import *
import numpy as np


def IceWatch_seg(img, model, classes, shape = 'normal', target_size = (256, 256)):
    '''
    IceWatch segmentation
    ======= Inputs ========
    img: input image
    model: pre-trained model
    shape: output image mode
    target_size: target size of output image      
    ======= Outputs ========
    pred: prediction by the model
    pred_rgb: colorized segmentation
        
    '''
    
    # Predict
    pred_mask = model.predict(np.expand_dims(img, 0))
    pred_mask = np.argmax(pred_mask, axis=-1)
    pred = []
    pred_rgb = []
    pred_num = len(pred_mask)  # the output of the deep supervised model has more than 1 prediction at different depths
        
    # Map the class names to RGB colours.
    cls2rgb = {cl: list(classes.loc[cl, :]) for cl in classes.index}
    # Map the index numbers to RGB colours
    idx2rgb = {idx: np.array(rgb) for idx, (cl, rgb) in enumerate(cls2rgb.items())}
    def map_class_to_rgb(p):
        return idx2rgb[p[0]]
    
    if pred_num == 1:    # pred_mask.shape = (1, target_size[0], target_shape[1])
        pred_mask = np.expand_dims(pred_mask, 0)   # pred_mask.shape = (1, 1, target_size[0], target_shape[1])
                                                   #                 = (pred_num, 1, target_size[0], target_shape[1])
        
    for i in range(pred_num):   
        pred_label = pred_mask[i][0]
                
        if shape == 'flat':
            pred_label = np.reshape(pred_label, target_size)
        
        rgb_mask = np.apply_along_axis(map_class_to_rgb, -1, np.expand_dims(pred_label, -1))
        
        pred.append(pred_label)   # model prediction
        pred_rgb.append(rgb_mask)   # colorized segmentation

    return pred, pred_rgb

