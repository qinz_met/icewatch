This is the README file for classification of Ice Watch (https://icewatch.met.no) photographs.

In addition to the Python scripts, a trained model data file is also needed. As this is too big for Bitbucket, please download from https://drive.google.com/file/d/1RbbDlDtnuqhLAp9y2H3L-yeSVHImS9VV/view?usp=sharing

The current trained UNet++ model outputs a 3-class segmentation of an Ice Watch image with pixel values of 0 for water, 1 for ice, and 2 for others (background). 

A class-to-colour mapping file is needed when producing a colorized segmentation (water in blue, ice in green, and others in black), please download from https://drive.google.com/file/d/1vZ-cgRLGxTPGElH4fMV5afubV5PuOA_p/view?usp=sharing

IceWatch_demo.py gives an example of how to run the Ice Watch image processing, including geometric calibration (orthorect.py).