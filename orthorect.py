#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 17:35:59 2021

@author: qinz
"""

import numpy as np
import cv2

def orthorect(img, pitch, roll, yaw, isRad = False, scale = 1, dx = 0, dy = 0, borderValue = None):  
    '''''
    pitch: rotation around the Y axis
    roll: rotation around the X axis
    yaw: rotation around the Z axis
    '''''
    h, w = img.shape[0], img.shape[1]  
    
    if isRad == False:
        pitch, roll, yaw = np.deg2rad(pitch), np.deg2rad(roll), np.deg2rad(yaw)
        # fov = np.deg2rad(fov)
    
    d = np.sqrt(w ** 2 + h ** 2) / scale
    f = d / (2 * np.sin(yaw) if np.sin(yaw) != 0 else 1)
    dz = f

    # Rotation matrices around the Y, X, and Z axis
    Ry = np.array([ [1, 0, 0, 0],
                    [0, np.cos(pitch), -np.sin(pitch), 0],
                    [0, np.sin(pitch), np.cos(pitch), 0],
                    [0, 0, 0, 1] ])
    
    Rx = np.array([ [np.cos(roll), 0, -np.sin(roll), 0],
                    [0, 1, 0, 0],
                    [np.sin(roll), 0, np.cos(roll), 0],
                    [0, 0, 0, 1] ])
    
    Rz = np.array([ [np.cos(yaw), -np.sin(yaw), 0, 0],
                    [np.sin(yaw), np.cos(yaw), 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1] ])    
    # Composed rotation matrix with (Ry, Rx, Rz)
    R = np.dot(np.dot(Ry, Rx), Rz)
                
    # Translation matrix, the optical center world coordinate system   
    T = np.array([  [1, 0, 0, dx],
                    [0, 1, 0, dy],
                    [0, 0, 1, dz],
                    [0, 0, 0,  1] ])

    # 3D-to-2D projection matrix
    H = np.array([ [f, 0, w/2, 0],
                   [0, f, h/2, 0],
                   [0, 0,   1, 0] ])
    # Mapping from world to camera coordinate frame
    M = np.dot(H, np.dot(T, R)) / f
    
    
    # 2D-to-3D projection matrix 
    # K0 is used to move image center to (0,0)
    K0 = np.array([ [1, 0, 0],
                    [0, 1, 0],
                    [0, 0, 1],
                    [0, 0, 1] ])
    
    # Transformation matrix
    warpR0 = np.dot(M, K0)
    warpR0 = np.linalg.inv(warpR0)
    
    # 4 corners of the source image
    pnts = np.array([ [0, 0],
                      [w, 0],
                      [w, h],
                      [0, h] ], np.float32)
    pnts = np.array([pnts])
    
    # Mapping of 4 corners from original image to destination image
    dst_pnts = cv2.perspectiveTransform(pnts, warpR0)
    dst_pnts = np.asarray(dst_pnts, dtype=np.float32)
    # Bounding box of 4 corners in destination
    bbox = cv2.boundingRect(dst_pnts)
    # Correct position of top-left corner of original image in the destination
    x_offset = bbox[0]
    y_offset = bbox[1]
    # Size of the destination image
    width = bbox[2]
    height = bbox[3]
    
    # K is used to move destination's top-left corner to the origin (0,0) in warped destination image
    K = np.array([ [1, 0, x_offset],
                   [0, 1, y_offset],
                   [0, 0,        1],
                   [0, 0,        1] ])
    
    # Final transformation matrix
    warpR = np.dot(M, K)
    warpR = np.linalg.inv(warpR)
    
    warpImg = cv2.warpPerspective(img, warpR, (width, height), flags = cv2.INTER_NEAREST, borderMode = cv2.BORDER_CONSTANT, borderValue = borderValue)
    
    return warpImg 



